from datetime import datetime
import re
import xlsxwriter
from openpyxl import load_workbook
from openpyxl.styles import Alignment
import requests
from bs4 import BeautifulSoup
from time import sleep
from operator import itemgetter
from tkinter import *
import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile
import numpy as np
import string
import math

# Current version of the program, to be displayed on GUI
VERSION_NUMBER = "1.80"

#Find the name of a column given a col number
def findCol(num):
    baseString = ""
    lastNum = num
    result = num

    while num > 0:
        remainder = num % 26

        if remainder == 1:
            baseString = baseString + "A"
        elif remainder == 2:
            baseString = baseString + "B"
        elif remainder == 3:
            baseString = baseString + "C"
        elif remainder == 4:
            baseString = baseString + "D"
        elif remainder == 5:
            baseString = baseString + "E"
        elif remainder == 6:
            baseString = baseString + "F"
        elif remainder == 7:
            baseString = baseString + "G"
        elif remainder == 8:
            baseString = baseString + "H"
        elif remainder == 9:
            baseString = baseString + "I"
        elif remainder == 10:
            baseString = baseString + "J"
        elif remainder == 11:
            baseString = baseString + "K"
        elif remainder == 12:
            baseString = baseString + "L"
        elif remainder == 13:
            baseString = baseString + "M"
        elif remainder == 14:
            baseString = baseString + "N"
        elif remainder == 15:
            baseString = baseString + "O"
        elif remainder == 16:
            baseString = baseString + "P"
        elif remainder == 17:
            baseString = baseString + "Q"
        elif remainder == 18:
            baseString = baseString + "R"
        elif remainder == 19:
            baseString = baseString + "S"
        elif remainder == 20:
            baseString = baseString + "T"
        elif remainder == 21:
            baseString = baseString + "U"
        elif remainder == 22:
            baseString = baseString + "V"
        elif remainder == 23:
            baseString = baseString + "W"
        elif remainder == 24:
            baseString = baseString + "X"
        elif remainder == 25:
            baseString = baseString + "Y"
        elif remainder == 0:
            baseString = baseString + "Z"

        num = num - 26
        if num >= 1 and num <= 26:
            num = 1
        elif num >= 27 and num <= 52:
            num = 2
        elif num >= 53 and num <= 78:
            num = 3
        elif num >= 79 and num <= 104:
            num = 4
        elif num >= 105 and num <= 130:
            num = 5
        elif num >= 131 and num <= 156:
            num = 6

    #reverse the string and return it
    return baseString[::-1]

#Finds the ordinal of a given number
def getOrdinalFor(num):
    hundredRemainer = num % 100
    tenRemainder = num % 10

    #if its in the teens
    if hundredRemainer - tenRemainder == 10:
        return str("th")

    if tenRemainder == 1:
        return str("st")
    elif tenRemainder == 2:
        return str("nd")
    elif tenRemainder == 3:
        return str("rd")
    else:
        return str("th")


class Calculator:

    #FIELDS
    resultsBook = 0
    Races = [] # online races with corresponding URLs
    Drivers = [] # results list of the current race
    Picks = [] # list of members with their corresponding picks
    Scores = [] # list of members with their corresponding scores for current race
    PlaceFinishes = [] # list of members with their corresponding list of place finishes
    memberStatsFull = [] # list of members with their name, current total score, their current pick, all of their previous scores, and their current points
    paymentStatus = [] # list of members with their payment status
    firstPlaceFinishes = [] # list of members and how many wins they have
    wildcardBreakdown = [] # list that shows how the wildcard was broken down
    driverFrequency = [] # list of drivers and the number of times they were picked
    # data = requests.get('http://www.espn.com/racing/results/_/year/2018')  #last years results for testing purposes
    data = requests.get('http://www.espn.com/racing/results')
    markedRaces = ["duel", "all-star", "all star", "clash"] # Set the marked races
    gSheet = pd.ExcelFile('key_sheets\MasterResults.xlsx')
    numMembers=0
    year=0
    currentRaceNum=0
    gap=0
    markedRace=0
    wildcard = ""


############################################################################################################################################################
############################################################################################################################################################
    #Finds the current year
    def findYear(self):
        date = str(datetime.date(datetime.now()))
        date = date[0:4]
        self.year = date

############################################################################################################################################################
############################################################################################################################################################

    #Method to fill the races list with track names and URLs from races
    def findOnlineRaces(self):
        print_to_log("Scraping all available races..")

        #load the data into bs4
        soup = BeautifulSoup(self.data.text, 'html.parser')
        data = []
        URLs = []
        for tr in soup.find_all('tr'):
            for td in tr.find_all('td'):
                for b in td.find_all('b'):
                    values = [a for a in b.find_all('a')]
                    #print(values)
                    data.append(values)

        #Scrape the soup for URLs
        i=0
        size = len(data)
        while i < size:
            v = str(data[i]) #get a line
            l = v.split('"')[1::2] #find URL inbetween quotes
            url = str(l)
            url = url[2:-2] #only get after first 2 chars and disregard last 2

            v = str(data[i]) #get the same line again
            track = v.split(">", 1)[1] #get everything after >
            track = track[:-5] #strip the last 5 chars

            #append the info to races struct
            race = track, url

            #skip over the moster energy open
            if track.lower() not in "monster energy open":
                self.Races.append(race)

            i=i+1

############################################################################################################################################################
############################################################################################################################################################

    #Finds the current race on the spreadsheet and sets currentRaceNum and gap
    def findCurSpreadsheetRace(self):

        df = self.gSheet.parse('MasterResults', usecols = "C", na_values=['NA'], header = None)
        df = df.replace(np.nan, 'EMPTY', regex=True)
        c = df[2].tolist()
        col = []
        for i in c:
            if i != 'EMPTY':
                col.append(i)
        self.currentRaceNum=len(col)
        if self.currentRaceNum < 3:
            self.currentRaceNum = self.currentRaceNum - 1
        #print(currentRaceNum)
        df = self.gSheet.parse('MasterResults', usecols = "A", na_values=['NA'], header = None)
        df = df.replace(np.nan, 'EMPTY', regex=True)
        c = df[0].tolist()
        counter = 1
        for i in c:
            if "Trophy" in str(i):
                break
            counter = counter + 1
        self.gap = counter

############################################################################################################################################################
############################################################################################################################################################

    #Loads the current race's results into the drivers list. Also determines if a race is marked and sets the flag
    def loadRaceResults(self):
        track, url = self.Races[self.currentRaceNum]
        track = track.lower()

        # print(track)
        print_to_log(str(track))

        #if there's a ' * ' next to the race, the scoring is different. Set a flag
        print_to_log("Checking if race marked with ' * '")
        for r in self.markedRaces:
            if str(r) in track:
                self.markedRace = 1
                #print("Alternate scoring enabled!")
                print_to_log("Marked, alternate scoring enabled!")
                break

        ##################################################################
        ########## NORMAL RACE ARRAY LOADING #############################
        ##################################################################
        if "duel" not in track:
            #load the data into bs4
            data = requests.get(url)
            soup = BeautifulSoup(data.text, 'html.parser')
            data = []
            #Drivers = []
            posFound = 0

            #loop through values
            for tr in soup.find_all('tr'):
                values = [td.text.lower() for td in tr.find_all('td')]
                if "pos" in values:
                    #get indices of values we care about
                    pos = values.index('pos')
                    driver = values.index('driver')
                    pts = values.index('pts')
                    penalty = values.index('penalty')
                    posFound=1

                if posFound == 1:
                    #get information we care about and add the new driver
                    p = values[pos]
                    d = values[driver]
                    pt = values[pts]
                    pen = values[penalty]
                    newDriver = d, p, pt, pen
                    self.Drivers.append(newDriver)
                    # print(newDriver)
                    #buffer.set(str(newDriver))
                    print_to_log(str(newDriver))

        ###################################################################

        ##################################################################
        ############ THE DUELS ARRAY LOADING #############################
        ##################################################################
        else:
            #load the data into bs4 from the first duel
            data = requests.get(url)
            soup = BeautifulSoup(data.text, 'html.parser')
            data = []
            #Drivers = []
            posFound = 0

            #loop through values
            for tr in soup.find_all('tr'):
                values = [td.text.lower() for td in tr.find_all('td')]
                if "pos" in values:
                    #get indices of values we care about
                    pos = values.index('pos')
                    driver = values.index('driver')
                    pts = values.index('pts')
                    penalty = values.index('penalty')
                    posFound=1

                if posFound == 1:
                    #get information we care about and add the new driver
                    p = values[pos]
                    d = values[driver]
                    pt = values[pts]
                    pen = values[penalty]
                    newDriver = d, p, pt, pen
                    self.Drivers.append(newDriver)
                    # print(newDriver)
                    print_to_log(str(newDriver))

            #get the second duel
            self.currentRaceNum = self.currentRaceNum + 1
            track, url = self.Races[self.currentRaceNum]
            track = track.lower()
            data = requests.get(url)
            soup = BeautifulSoup(data.text, 'html.parser')
            posFound = 0

            #loop through values
            for tr in soup.find_all('tr'):
                values = [td.text.lower() for td in tr.find_all('td')]
                if "pos" in values:
                    #get indices of values we care about
                    pos = values.index('pos')
                    driver = values.index('driver')
                    pts = values.index('pts')
                    penalty = values.index('penalty')
                    posFound=1

                if posFound == 1:
                    #get information we care about and add the new driver
                    p = values[pos]
                    d = values[driver]
                    pt = values[pts]
                    pen = values[penalty]
                    newDriver = d, p, pt, pen
                    self.Drivers.append(newDriver)
                    # print(newDriver)
                    print_to_log(str(newDriver))

        ###################################################################


        print_to_log("")
        print_to_log("")

############################################################################################################################################################
############################################################################################################################################################

    #creeates the new spreadsheet
    def createWorkbook(self):
        #Add new excel workbook and sheet
        print_to_log("Creating new Excel Spreadsheet..")
        track, url = self.Races[self.currentRaceNum]
        track = track.lower()

        fileName = str(track + " Results.xlsx")
        self.resultsBook = xlsxwriter.Workbook(fileName)









        #Create each spreadsheet
        self.createRuleSheet()
        self.createPicksSheet()
        self.createStandingsSheet()
        self.createPlaceFinishesSheet()

        root.after(1500)
        self.resultsBook.close()

############################################################################################################################################################
############################################################################################################################################################

    def createRuleSheet(self):
        ruleSheet = self.resultsBook.add_worksheet('Rules')

        #Do some formatting
        ruleSheet.merge_range('A1:D1', 'Merged Range')
        ruleSheet.set_column('A:A', 45)
        ruleSheet.set_column('B:B', 15)
        ruleSheet.set_column('C:C', 20)
        ruleSheet.set_column('D:D', 15)
        ruleSheet.merge_range('B2:D2', 'Merged Range')

        title_format              = self.resultsBook.add_format({'bold': True, 'font_size':14, 'font_name': 'Times New Roman', 'font_color': 'black', 'align': 'center'})
        fee_format                = self.resultsBook.add_format({'font_size':9, 'font_name': 'Times New Roman', 'font_color': 'green'})
        name_format               = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'blue', 'align': 'left'})
        rules_format              = self.resultsBook.add_format({'font_size':9, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'left'})

        #Create the rule sheet and copy from stored rules sheet
        print_to_log("Reading stored rules sheet..")
        ruleBook_Read = pd.ExcelFile('key_sheets\Rules.xlsx')
        df = ruleBook_Read.parse('Rules', usecols = "A", na_values=['NA'], header = None)
        df = df.replace(np.nan, 'EMPTY', regex=True)
        col = df[0].tolist()
        col = filter(lambda a: a != "EMPTY", col)

        print_to_log("Copying rules sheet to new doc..")
        ruleNum=0
        for rule in col:
            if ruleNum == 0:
                ruleSheet.write(ruleNum, 0, str(rule), title_format)
            elif ruleNum == 1:
                ruleSheet.write(ruleNum, 0, str(rule), fee_format)
                ruleSheet.write(ruleNum, 1, str("Name: "), name_format)
            else:
                s = str("A" + str(ruleNum+1) + ":D" + str(ruleNum+1))
                ruleSheet.merge_range(s, ' ')

                if ruleNum == 4:
                    ruleSheet.set_row(ruleNum-1, 36)
                elif ruleNum == 7:
                    ruleSheet.set_row(ruleNum-1, 55)
                elif ruleNum == 8 or ruleNum == 13:
                    ruleSheet.set_row(ruleNum-1, 26)
                else:
                    ruleSheet.set_row(ruleNum+1, 13)

                ruleSheet.write(ruleNum, 0, str(rule), rules_format)
            ruleNum = ruleNum + 1

############################################################################################################################################################
############################################################################################################################################################

    def createPicksSheet(self):
        size = self.numMembers

        #Copy the stored picks sheet to the new sheet
        picksSheet = self.resultsBook.add_worksheet('Picks')

        #Do some text formatting
        picksTitle_format         = self.resultsBook.add_format({'font_size':18, 'font_name': 'Wide Latin', 'text_wrap': True, 'align': 'center', 'font_color':'black', 'bg_color': '#A9A9A9'})
        text_format_white         = self.resultsBook.add_format({'font_size':9, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'left', 'border':1})
        text_format_grey          = self.resultsBook.add_format({'font_size':9, 'font_name': 'Times New Roman', 'font_color': 'black', 'bg_color': '#A9A9A9', 'text_wrap': True, 'align': 'left', 'border':1})
        text_format_yellow        = self.resultsBook.add_format({'font_size':9, 'font_name': 'Times New Roman', 'font_color': 'black', 'bg_color': 'yellow', 'text_wrap': True, 'align': 'left', 'border':1})
        date_format               = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'text_wrap': True, 'align': 'right', 'border':1})
        date_format_grey          = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'text_wrap': True, 'bg_color': '#A9A9A9', 'align': 'right', 'border':1})
        date_format_yellow        = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'text_wrap': True, 'bg_color': 'yellow', 'align': 'right', 'border':1})
        race_format               = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'text_wrap': True, 'align': 'left', 'border':1, 'right':2})
        race_format_grey          = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'text_wrap': True, 'bg_color': '#A9A9A9', 'align': 'left', 'border':1, 'right':2})
        race_format_yellow        = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'text_wrap': True, 'bg_color': 'yellow', 'align': 'left', 'border':1, 'right':2})
        trophy_format2            = self.resultsBook.add_format({'bold': True, 'font_size':12, 'font_name': 'Times New Roman', 'bg_color': '00FF00', 'text_wrap': True, 'align': 'center', 'border':1})
        text_format_white_center  = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'border':1})
        text_format_grey_center   = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'bg_color': '#A9A9A9', 'text_wrap': True, 'align': 'center', 'border':1})
        text_format_yellow_center = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'bg_color': 'yellow', 'text_wrap': True, 'align': 'center', 'border':1})
        namesRow_format           = self.resultsBook.add_format({'bold': True, 'font_size':9, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'bottom':2, 'top':2, 'right':1, 'left':1})
        payment_format            = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'text_wrap': True, 'align': 'center', 'font_color':'green'})
        noteLabel_format          = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center'})
        noteLabel_yellow          = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'bg_color': 'yellow', 'text_wrap': True, 'align': 'center'})

        #Format the top of the picks sheet
        picksSheet.set_column('B:B', 18)
        picksSheet.merge_range('A1:B1', '')
        c = findCol(size+2)
        s = str("C1:" + str(c) + "2")
        picksSheet.merge_range(s, ' ')
        picksSheet.write(0, 0, str(""), picksTitle_format)
        picksSheet.write(1, 0, str(""), picksTitle_format)
        picksSheet.write(1, 1, str(""), picksTitle_format)
        picksSheet.write(2, 0, str(""), picksTitle_format)
        picksSheet.write(2, 1, str(""), picksTitle_format)
        s = str("NASCAR - ") + str(self.year) + str(" Monster Energy Cup Picks")
        picksSheet.write(0, 2, s, picksTitle_format)

        #Copy outer shell to picks sheet
        print_to_log("Copying outer shell of master to standings sheet..")
        r=5
        c=1
        df = self.gSheet.parse('Picks', usecols = "A", na_values=['NA'], header = None)
        df = df.replace(np.nan, 'EMPTY', regex=True)
        co = df[0].tolist()
        col = []
        for i in co:
            if str(i) != 'EMPTY':
                col.append(str(i))

        dateNum=0
        gapAdjusted=0
        dateNum2=0

        #copy dates
        i=1
        for date in col:
            s = str(date)
            if "Trophy" in str(s):
                picksSheet.set_row(dateNum+r-1, 40)
                for j in range(size+2):
                    picksSheet.write(dateNum+r-1, j, str(""), trophy_format2)

                s2 = str("A" + str(dateNum+r) + ":B" + str(dateNum+r))
                picksSheet.merge_range(s2, 'Merged Range')
                picksSheet.write(dateNum+r-1, 0, str(s), trophy_format2)
                gapAdjusted=dateNum
            elif "Date" in str(s):
                picksSheet.write(dateNum+r-1, 0, str(s), text_format_white)
                i=i+1
                dateNum2=dateNum2+1
            else:
                s = s[5:-9]
                if i%3 == 0 and i != 0:
                    picksSheet.write(dateNum+r-1, 0, str(s), date_format_grey)
                else:
                    picksSheet.write(dateNum+r-1, 0, str(s), date_format)
                i=i+1
                dateNum2=dateNum2+1


            #overwrite the date with yellow background if its the current race
            if self.currentRaceNum+1 == dateNum2 and "Trophy" not in str(s) and self.currentRaceNum != 0:
                picksSheet.write(dateNum+r-1, 0, str(s), date_format_yellow)

            #adjust for first race
            if dateNum == 1 and self.currentRaceNum == 0:
                picksSheet.write(dateNum+r-1, 0, str(s), date_format_yellow)

            dateNum = dateNum + 1


        r=4
        df = self.gSheet.parse('Picks', usecols = "B", na_values=['NA'], header = None)
        df = df.replace(np.nan, 'EMPTY', regex=True)
        co = df[1].tolist()
        col = []
        for i in co:
            if str(i) != 'EMPTY':
                col.append(str(i))

        raceNum=0
        raceNum2=0
        totalsFound=0
        text_format_white.set_font_size(10)
        text_format_grey.set_font_size(10)

        #Set race locations, totals and diff labels
        i=0
        for race in col:
            s = str(race)
            if raceNum+1 == self.gap:
                raceNum = raceNum + 1
            if "Totals" in str(s) or totalsFound == 1:
                #picksSheet.write(raceNum+r-1, 1, str(s), totals_format)
                totalsFound=1
            else:
                if i%3 == 0 and i != 0:
                    picksSheet.write(raceNum+r-1, 1, str(s), race_format_grey)
                else:
                    picksSheet.write(raceNum+r-1, 1, str(s), race_format)

            if raceNum2 == self.currentRaceNum+1 and self.currentRaceNum != 0:
                picksSheet.write(raceNum+r-1, 1, str(s), race_format_yellow)

            #adjust for first race
            if raceNum2 == 2 and self.currentRaceNum == 0:
                picksSheet.write(raceNum+r-1, 1, str(s), race_format_yellow)

            raceNum2 = raceNum2 + 1
            raceNum = raceNum + 1
            i=i+1


        #put a note in at the bottom denoting what the yellow row means
        picksSheet.write(raceNum+r+1, 2, str("NOTES"), noteLabel_format)
        picksSheet.write(raceNum+r+2, 2, str("This weeks Race"), noteLabel_yellow)
        print_to_log("Reading stored picks sheet..")


        #loop through each member, grab their picks list from the picks sheet,
        #paste into new picks sheet
        c=3
        i=0
        size = len(self.memberStatsFull)
        while i < size:

            #copy the member's column into  a list
            colLetter = findCol(c+i)
            picksSheet.set_column(c+i-1, c+i-1, 16)
            dfPicks = self.gSheet.parse('Picks', usecols = colLetter, na_values=['NA'], header = None)
            dfPicks = dfPicks.replace(np.nan, ' ', regex=True)
            col = dfPicks[c+i-1]
            sheetPicks = []
            for n, entry in enumerate(col):
                if n+1 == self.gap:
                    sheetPicks.append("GAP")
                else:
                    sheetPicks.append(entry)



            #Paste the column into new picks sheet
            n=0
            m=0
            extraBit=0
            for val in sheetPicks:
                if n+1 == self.gap:
                    n = n + 1
                    extraBit=1
                else:
                    #Print the names row differently, and print if they've paid above it
                    if n==0:
                        picksSheet.write(n+r-1, c+i-1, str(val), namesRow_format)
                        picksSheet.write(n+r-2, c+i-1, str(self.paymentStatus[i][1]), payment_format)

                    #adjust for the first race if its the current one, skip a cell and paint it yellow
                    elif self.currentRaceNum == 0 and n == 2:
                        picksSheet.write(n+r-1, c+i-1, str(val), text_format_yellow_center)
                    else:
                        #if it's every third row, make it grey
                        if (n-extraBit)%3 == 0 and n != 0:
                            #if its the current race and the current race num isnt 0, paint it yellow. else, grey
                            if m == self.currentRaceNum+1 and self.currentRaceNum != 0:
                                picksSheet.write(n+r-1, c+i-1, str(val), text_format_yellow_center)
                            else:
                                picksSheet.write(n+r-1, c+i-1, str(val), text_format_grey_center)
                        else:
                            #if its the current race and the current race num isnt 0, paint it yellow. else, white
                            if m == self.currentRaceNum+1 and self.currentRaceNum != 0:
                                picksSheet.write(n+r-1, c+i-1, str(val), text_format_yellow_center)
                            else:
                                picksSheet.write(n+r-1, c+i-1, str(val), text_format_white_center)
                    n=n+1
                    m=m+1

            i=i+1
        # Freeze the top rows
        picksSheet.freeze_panes(4, 0)

############################################################################################################################################################
############################################################################################################################################################

    def createStandingsSheet(self):
        #Create the standings sheet
        print_to_log("Creating Standings Sheet..")
        standingsSheet = self.resultsBook.add_worksheet('Standings Regular Season')

        #Do some text formatting
        picksTitle_format         = self.resultsBook.add_format({'font_size':18, 'font_name': 'Wide Latin', 'text_wrap': True, 'align': 'center', 'font_color':'black', 'bg_color': '#A9A9A9'})
        text_format_white         = self.resultsBook.add_format({'font_size':9, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'left', 'border':1})
        text_format_grey          = self.resultsBook.add_format({'font_size':9, 'font_name': 'Times New Roman', 'font_color': 'black', 'bg_color': '#A9A9A9', 'text_wrap': True, 'align': 'left', 'border':1})
        text_format_yellow        = self.resultsBook.add_format({'font_size':9, 'font_name': 'Times New Roman', 'font_color': 'black', 'bg_color': 'yellow', 'text_wrap': True, 'align': 'left', 'border':1})
        date_format               = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'text_wrap': True, 'align': 'right', 'border':1})
        date_format_grey          = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'text_wrap': True, 'bg_color': '#A9A9A9', 'align': 'right', 'border':1})
        date_format_yellow        = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'text_wrap': True, 'bg_color': 'yellow', 'align': 'right', 'border':1})
        race_format               = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'text_wrap': True, 'align': 'left', 'border':1, 'right':2})
        race_format_grey          = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'text_wrap': True, 'bg_color': '#A9A9A9', 'align': 'left', 'border':1, 'right':2})
        race_format_yellow        = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'text_wrap': True, 'bg_color': 'yellow', 'align': 'left', 'border':1, 'right':2})
        text_format_white_center  = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'border':1})
        text_format_grey_center   = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'bg_color': '#A9A9A9', 'text_wrap': True, 'align': 'center', 'border':1})
        text_format_yellow_center = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'bg_color': 'yellow', 'text_wrap': True, 'align': 'center', 'border':1})
        namesRow_format           = self.resultsBook.add_format({'bold': True, 'font_size':9, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'bottom':2, 'top':2, 'right':1, 'left':1})
        trophy_format             = self.resultsBook.add_format({'bold': True, 'font_size':12, 'font_name': 'Times New Roman', 'bg_color': 'red', 'text_wrap': True, 'align': 'center', 'border':1})
        totals_format             = self.resultsBook.add_format({'font_size':9, 'font_name': 'Times New Roman', 'text_wrap': True, 'align': 'right', 'font_color': '#800000'})
        chase_place_format        = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'text_wrap': True, 'align': 'center', 'font_color':'black', 'bg_color':'#00FF00'})
        chase_place_format_BL     = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'text_wrap': True, 'align': 'center', 'font_color':'black', 'bg_color':'#00FF00', 'left':2})
        chase_place_format_BR     = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'text_wrap': True, 'align': 'center', 'font_color':'black', 'bg_color':'#00FF00', 'right':2})
        place_format              = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'text_wrap': True, 'align': 'center', 'font_color':'black', 'border':0})
        chase_title_format        = self.resultsBook.add_format({'bold': True, 'font_size':14, 'font_name': 'Wide Latin', 'text_wrap': True, 'align': 'center', 'font_color':'black', 'bg_color':'#00FF00', 'top':2})
        chase_title_format_TL     = self.resultsBook.add_format({'bold': True, 'font_size':14, 'font_name': 'Times New Roman', 'text_wrap': True, 'align': 'center', 'font_color':'black', 'bg_color':'#00FF00', 'top':2, 'left':2})
        chase_title_format_TR     = self.resultsBook.add_format({'bold': True, 'font_size':14, 'font_name': 'Times New Roman', 'text_wrap': True, 'align': 'center', 'font_color':'black', 'bg_color':'#00FF00', 'top':2, 'right':2})
        chase_border_name         = self.resultsBook.add_format({'bold': True, 'font_size':9, 'font_name': 'Times New Roman', 'text_wrap': True, 'align': 'center', 'font_color':'black', 'right':2, 'top':2, 'bottom':2, 'left':1})
        chase_border_value        = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'text_wrap': True, 'align': 'center', 'font_color':'black', 'right':2, 'top':1, 'bottom':1, 'left':1 })
        chase_border_value_grey   = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'text_wrap': True, 'align': 'center', 'font_color':'black', 'right':2, 'top':1, 'bottom':1, 'left':1, 'bg_color':'#A9A9A9'})
        standingsTitle_format     = self.resultsBook.add_format({'bold': True, 'font_size':18, 'font_name': 'Times New Roman', 'text_wrap': True, 'align': 'center', 'font_color':'black'})
        pointsBehindNlabel_format = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'left', 'bg_color': '#00FF00'})
        pointsBehindNval_format   = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': '#00FF00', 'text_wrap': True, 'align': 'center', 'border':1})
        scores_format             = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'left':1, 'right':1})
        totalsValue_format        = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'left':1, 'right':1, 'top':2})
        noteLabel_yellow          = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'bg_color': 'yellow', 'text_wrap': True, 'align': 'center'})
        noteLabel_blue            = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'bg_color': '#00E1E1', 'text_wrap': True, 'align': 'center'})
        noteLabel_blueText        = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': '#00E1E1', 'text_wrap': True, 'align': 'center'})
        wildcardName_format       = self.resultsBook.add_format({'bold': True, 'font_size':9, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'bottom':2, 'top':2, 'right':1, 'left':1, 'bg_color': '#00E1E1'})
        frequencies_title         = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'right':2, 'bg_color': '#9999FF', 'bold':True})
        zero_wins_white           = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'border':1, 'top':2, 'bg_color': '#FFFFFF'})
        one_win_peach             = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'border':1, 'top':2, 'bg_color': '#FFE5B4'})
        two_wins_lightBlue        = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'border':1, 'top':2, 'bg_color': '#4EE2EC'})
        three_wins_yellowGreen    = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'border':1, 'top':2, 'bg_color': '#52D017'})
        four_wins_beer            = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'border':1, 'top':2, 'bg_color': '#FBB117'})
        five_wins_lightSlateBlue  = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'border':1, 'top':2, 'bg_color': '#736AFF'})
        six_wins_mangoOrange      = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'border':1, 'top':2, 'bg_color': '#FF8040'})
        seven_wins_hotPink        = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'border':1, 'top':2, 'bg_color': '#F660AB'})
        eight_wins_purpleDragon   = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'border':1, 'top':2, 'bg_color': '#C38EC7'})
        ninePlus_wins_red         = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'border':1, 'top':2, 'bg_color': '#FF0000'})

        standingsSheet.set_column('B:B', 18)

        #Copy outer shell to standings sheet
        print_to_log("Copying outer shell of master to standings sheet..")
        r=4
        c=1
        df = self.gSheet.parse('MasterResults', usecols = "A", na_values=['NA'], header = None)
        df = df.replace(np.nan, 'EMPTY', regex=True)
        co = df[0].tolist()
        col = []
        for i in co:
            if str(i) != 'EMPTY':
                col.append(str(i))

        dateNum=0
        gapAdjusted=0

        #copy dates
        i=0
        for date in col:
            s = str(date)
            if "Trophy" in str(s):
                standingsSheet.set_row(dateNum+r-1, 40)
                for j in range(self.numMembers+2):
                    standingsSheet.write(dateNum+r-1, j, str(""), trophy_format)

                #standingsSheet.set_row(dateNum+r-1, 40, cell_format=trophy_format)
                s2 = str("A" + str(dateNum+r) + ":B" + str(dateNum+r))
                standingsSheet.merge_range(s2, 'Merged Range')
                standingsSheet.write(dateNum+r-1, 0, str(s), trophy_format)
                gapAdjusted=dateNum
            elif "Date" in str(s):
                standingsSheet.write(dateNum+r-1, 0, str(s), text_format_white)
                i=i+1
            else:
                s = s[5:-9]
                if i%3 == 0 and i != 0:
                    standingsSheet.write(dateNum+r-1, 0, str(s), date_format_grey)
                else:
                    standingsSheet.write(dateNum+r-1, 0, str(s), date_format)
                i=i+1
            dateNum = dateNum + 1

        df = self.gSheet.parse('MasterResults', usecols = "B", na_values=['NA'], header = None)
        df = df.replace(np.nan, 'EMPTY', regex=True)
        co = df[1].tolist()
        col = []
        for i in co:
            if str(i) != 'EMPTY':
                col.append(str(i))

        raceNum=0
        totalsFound=0
        text_format_white.set_font_size(10)
        text_format_grey.set_font_size(10)

        #Set race locations, totals and diff labels
        i=0
        for race in col:
            s = str(race)
            if raceNum+1 == self.gap:
                raceNum = raceNum + 1
            if "Totals" in str(s) or totalsFound == 1:
                standingsSheet.write(raceNum+r-1, 1, str(s), totals_format)
                totalsFound=1
            else:
                if i%3 == 0 and i != 0:
                    standingsSheet.write(raceNum+r-1, 1, str(s), race_format_grey)
                else:
                    standingsSheet.write(raceNum+r-1, 1, str(s), race_format)
            raceNum = raceNum + 1
            i=i+1

        #Find where the totals row is on the master doc
        df = self.gSheet.parse('MasterResults', usecols = "B", na_values=['NA'], header = None)
        df = df.replace(np.nan, 'EMPTY', regex=True)
        c = df[1].tolist()
        counter = 1
        for i in c:
            if "Totals:" in str(i):
                break
            counter = counter + 1
        totalsRow = counter

        #Find out how many members can be in the chase
        numChaseParticipants = math.floor(self.numMembers/2)
        if numChaseParticipants > 15:
            numChaseParticipants = 15

        #Put the "Diff from chase label in"
        whiteSpace=3
        c1 = findCol(numChaseParticipants-1+2)
        c2 = findCol(numChaseParticipants+2)
        s = str(c1) + str(totalsRow+4+whiteSpace) + str(":") + str(c2) + str(totalsRow+4+whiteSpace)
        standingsSheet.merge_range(s, ' ')
        print(s)
        ordinal = getOrdinalFor(numChaseParticipants)
        s = str("Points behind ") + str(numChaseParticipants) + str(ordinal)
        standingsSheet.write(totalsRow+3+whiteSpace, numChaseParticipants, s, pointsBehindNlabel_format)

        #merge the top row for title, and print title
        standingsSheet.merge_range('A1:B1', ' ')
        c = findCol(self.numMembers+2)
        s = str("C1:") + str(c) + str("1")
        standingsSheet.merge_range(s, ' ')
        s = str("NASCAR - ") + str(self.year) + str(" Monster Energy Cup Standings")
        standingsSheet.write(0, 2, s, standingsTitle_format)
        standingsSheet.set_row(0, 25)

        #color and border the chase label
        for n in range(numChaseParticipants):
            if n == 0:
                standingsSheet.write(1, 2+n, str(" "), chase_title_format_TL)
            elif n == numChaseParticipants-1:
                standingsSheet.write(1, 2+n, str(" "), chase_title_format_TR)
            else:
                standingsSheet.write(1, 2+n, str(" "), chase_title_format)

        c = findCol(numChaseParticipants+2)
        s = str("C2:") + str(c) + str("2")
        standingsSheet.merge_range(s, ' ')
        standingsSheet.write(1, 2, str("In The Chase"), chase_title_format_TL) #put chase title in

        #put a note in at the top denoting blue means wildcard
        standingsSheet.write(1, 2+numChaseParticipants+1, str(" "), noteLabel_blue)
        standingsSheet.write_string(1, 2+numChaseParticipants+2, str("=Wild Card"), noteLabel_blueText)

        #Copy all scores to new doc, in order based on total score
        whiteSpace=3
        i=0
        c=3
        firstPlaceScore=0
        secondPlaceScore=0
        thirdPlaceScore=0
        lastChaseScore=0
        size = len(self.memberStatsFull)
        while i < size:
            name, totalScore, pick, pts, scores = self.memberStatsFull[i]

            #get some specific place scores
            if i == 0:
                firstPlaceScore = totalScore
            if i == 1:
                secondPlaceScore = totalScore
            if i == 2:
                thirdPlaceScore = totalScore
            if i+1 == numChaseParticipants:
                lastChaseScore = totalScore

            #Loop through all scores, find first "EMPTY_CELL", replace with current race score.
            #makes copying easier
            for n, val in enumerate(scores):
                if val == "EMPTY_CELL":
                    scores[n] = int(pts)
                    break

            #print(scores)

            #iterate thru members old scores and put them in new doc
            firstPass=1
            r=4

            if i+1 == numChaseParticipants:
                standingsSheet.write(r-1, c+i-1, str(name), chase_border_name) #put name in
            else:
                if str(name) == self.wildcard:
                    standingsSheet.write(r-1, c+i-1, str(name), wildcardName_format) #put wildcard's name in
                else:
                    standingsSheet.write(r-1, c+i-1, str(name), namesRow_format) #put name in

            if i+1 <= numChaseParticipants:
                #put place in
                if i == 0:
                    standingsSheet.write(r-2, c+i-1, int(i+1), chase_place_format_BL)
                elif i == numChaseParticipants-1:
                    standingsSheet.write(r-2, c+i-1, int(i+1), chase_place_format_BR)
                else:
                    standingsSheet.write(r-2, c+i-1, int(i+1), chase_place_format)
            else:
                standingsSheet.write(r-2, c+i-1, int(i+1), place_format) #put place in

            r=r+1
            n=1
            for val in scores:

                #fill in score for every previous race in new results sheet
                if val != "GAP" and val != "EMPTY_CELL":
                    if n%3 == 0 and n != 0:
                        if i+1 == numChaseParticipants:
                            standingsSheet.write(r-1, c+i-1, int(val), chase_border_value_grey)
                        else:
                            standingsSheet.write(r-1, c+i-1, int(val), text_format_grey_center)
                    else:
                        if i+1 == numChaseParticipants:
                            standingsSheet.write(r-1, c+i-1, int(val), chase_border_value)
                        else:
                            standingsSheet.write(r-1, c+i-1, int(val), text_format_white_center)
                    r=r+1
                    n=n+1
                elif val == "GAP":
                    #print("t_format", r-1)
                    standingsSheet.write(r-1, c+i-1, str(""), trophy_format)
                    r=r+1
                else:
                    if n%3 == 0 and n != 0:
                        if i+1 == numChaseParticipants:
                            standingsSheet.write(r-1, c+i-1, str(""), chase_border_value_grey)
                        else:
                            standingsSheet.write(r-1, c+i-1, str(""), text_format_grey_center)
                    else:
                        if i+1 == numChaseParticipants:
                            standingsSheet.write(r-1, c+i-1, str(""), chase_border_value)
                        else:
                            standingsSheet.write(r-1, c+i-1, str(""), text_format_white_center)

                    if n+1 >= totalsRow:
                        standingsSheet.write(r-1, c+i-1, str(""), text_format_white_center)

                    r=r+1
                    n=n+1

            #put the current race results in new doc
            #standingsSheet.write(r-1, c+i-1, int(pts), text_format_white)

            #put the totals and differences from 1st, 2nd, and 3rd in
            standingsSheet.write(totalsRow+whiteSpace-1, c+i-1, totalScore, totalsValue_format)
            if i > 0:
                #diff from first
                standingsSheet.write(totalsRow+whiteSpace, c+i-1, firstPlaceScore-totalScore, scores_format)
            if i > 1:
                #diff from second
                standingsSheet.write(totalsRow+1+whiteSpace, c+i-1, secondPlaceScore-totalScore, scores_format)
            if i > 2:
                #diff from third
                standingsSheet.write(totalsRow+2+whiteSpace, c+i-1, thirdPlaceScore-totalScore, scores_format)
            if i+1 > numChaseParticipants:
                #diff from the chase
                standingsSheet.write(totalsRow+3+whiteSpace, c+i-1,lastChaseScore-totalScore, pointsBehindNval_format)


            #format the cells below first, second, and third that dont have values
            if i == 0:
                standingsSheet.write(totalsRow+whiteSpace, c+i-1, str(" "), scores_format)
                standingsSheet.write(totalsRow+whiteSpace+1, c+i-1, str(" "), scores_format)
                standingsSheet.write(totalsRow+whiteSpace+2, c+i-1, str(" "), scores_format)
            if i == 1:
                standingsSheet.write(totalsRow+whiteSpace+1, c+i-1, str(" "), scores_format)
                standingsSheet.write(totalsRow+whiteSpace+2, c+i-1, str(" "), scores_format)
            if i == 2:
                standingsSheet.write(totalsRow+whiteSpace+2, c+i-1, str(" "), scores_format)

            i=i+1

        frequencyRow = totalsRow + 9
        c = 2
        standingsSheet.write(frequencyRow, 1, "Pick Frequency:", frequencies_title)
        for driver in self.driverFrequency:
            name, freq = driver

            if freq == 0:
                standingsSheet.write(frequencyRow-1, c, str(name), zero_wins_white)
            elif freq == 1:
                standingsSheet.write(frequencyRow-1, c, str(name), one_win_peach)
            elif freq == 2:
                standingsSheet.write(frequencyRow-1, c, str(name), two_wins_lightBlue)
            elif freq == 3:
                standingsSheet.write(frequencyRow-1, c, str(name), three_wins_yellowGreen)
            elif freq == 4:
                standingsSheet.write(frequencyRow-1, c, str(name), four_wins_beer)
            elif freq == 5:
                standingsSheet.write(frequencyRow-1, c, str(name), five_wins_lightSlateBlue)
            elif freq == 6:
                standingsSheet.write(frequencyRow-1, c, str(name), six_wins_mangoOrange)
            elif freq == 7:
                standingsSheet.write(frequencyRow-1, c, str(name), seven_wins_hotPink)
            elif freq == 8:
                standingsSheet.write(frequencyRow-1, c, str(name), eight_wins_purpleDragon)
            else:
                standingsSheet.write(frequencyRow-1, c, str(name), ninePlus_wins_red)

            standingsSheet.write(frequencyRow, c, str(freq), text_format_white_center)
            c = c + 1


        #put a note in at bottom denoting yellow means penalty
        standingsSheet.write(frequencyRow + 2, 2, str("Penalty"), noteLabel_yellow)

        # Freeze the top rows
        standingsSheet.freeze_panes(4, 0)

############################################################################################################################################################
############################################################################################################################################################

    def createPlaceFinishesSheet(self):
        # Sort the place finishes array on the average key
        sortedPlaceFinishes = sorted(self.PlaceFinishes, key=itemgetter(1))
        # print(sortedPlaceFinishes)

        # Add the sheet to the workbook
        pfSheet = self.resultsBook.add_worksheet('Place Finishes')

        # Formatting
        title_format                       = self.resultsBook.add_format({'font_size':18, 'font_name': 'Wide Latin',      'text_wrap': True, 'align': 'center', 'font_color':'black'})
        text_format_white                  = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'left', 'border':1})
        race_label                         = self.resultsBook.add_format({'font_size':9,  'font_name': 'Times New Roman', 'font_color': '#003366', 'text_wrap': True, 'align': 'left', 'border':1, 'right':2, 'bold':True})
        date_label                         = self.resultsBook.add_format({'font_size':9,  'font_name': 'Times New Roman', 'font_color': '#003366', 'text_wrap': True, 'align': 'center', 'border':1, 'bold':True})
        name_label                         = self.resultsBook.add_format({'font_size':10,  'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'border':1, 'bold':True})
        text_format_white_center           = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'border':1})
        averages_row_format                = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'border':1, 'bg_color': '#00FFFF'})
        place_format                       = self.resultsBook.add_format({'font_size':10, 'font_name': 'Wide Latin',      'font_color': 'black', 'text_wrap': True, 'align': 'center', 'bottom':2, 'bold':True})
        race_format                        = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'text_wrap': True, 'align': 'left', 'border':1, 'right':2})
        race_score                         = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'left':1, 'right':1, 'bg_color': '#C0C0C0', 'bottom':7})
        averages_by_race                   = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'left':1, 'right':2, 'bg_color': '#46d246'})
        zero_wins_white                    = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'border':1, 'top':2, 'bg_color': '#FFFFFF'})
        one_win_peach                      = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'border':1, 'top':2, 'bg_color': '#FFE5B4'})
        two_wins_lightBlue                 = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'border':1, 'top':2, 'bg_color': '#4EE2EC'})
        three_wins_yellowGreen             = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'border':1, 'top':2, 'bg_color': '#52D017'})
        four_wins_beer                     = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'border':1, 'top':2, 'bg_color': '#FBB117'})
        five_wins_lightSlateBlue           = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'border':1, 'top':2, 'bg_color': '#736AFF'})
        six_wins_mangoOrange               = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'border':1, 'top':2, 'bg_color': '#FF8040'})
        seven_wins_hotPink                 = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'border':1, 'top':2, 'bg_color': '#F660AB'})
        eight_wins_purpleDragon            = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'border':1, 'top':2, 'bg_color': '#C38EC7'})
        ninePlus_wins_red                  = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'border':1, 'top':2, 'bg_color': '#FF0000'})
        wildcard_breakdown_title           = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'bottom':2, 'bg_color': 'yellow', 'bold':True})
        wildcard_breakdown_new_row         = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'border':1, 'bg_color': '#C38EC7', 'bold':True})
        wildcard_breakdown_name            = self.resultsBook.add_format({'font_size':10, 'font_name': 'Times New Roman', 'font_color': 'black', 'text_wrap': True, 'align': 'center', 'top':1, 'bottom':1})

        # merge title cells and put title in
        c = findCol(self.numMembers+2)
        s = str("C1:") + str(c) + str("1")
        pfSheet.merge_range(s, ' ')
        s = str("NASCAR - ") + str(self.year) + str(" Monster Energy Cup Place Finishes")
        pfSheet.write(0, 2, s, title_format)
        pfSheet.set_row(0, 25)

        # Copy dates from key sheet
        df = self.gSheet.parse('PlaceFinishes', usecols = "A", na_values=['EMPTY'], header = None)
        df = df.replace(np.nan, 'EMPTY', regex=True)
        co = df[0].tolist()
        col = []
        for i in co:
            if str(i) != 'EMPTY':
                col.append(str(i))

        # Print dates into new sheet
        r = 2
        for date in col:
            dateStr = str(date)
            if "Date" in dateStr:
                pfSheet.write(r, 0, dateStr, date_label)
            else:
                pfSheet.write(r, 0, dateStr[5:-9], text_format_white_center)
            r = r + 1

        # Copy race locations from key sheet
        df = self.gSheet.parse('PlaceFinishes', usecols = "B", na_values=['EMPTY'], header = None)
        df = df.replace(np.nan, 'EMPTY', regex=True)
        co = df[1].tolist()
        col = []
        for i in co:
            if str(i) != 'EMPTY':
                col.append(str(i))

        # Set width of race location column
        pfSheet.set_column('B:B', 15)

        # Print locations into new sheet
        r = 2
        for race in col:
            raceStr = str(race)
            if "Location" in raceStr:
                pfSheet.write(r, 1, raceStr, race_label)
            else:
                pfSheet.write(r, 1, raceStr, race_format)
            r = r + 1

        # Print a label for "Average Finish", and take note of the row
        avgFinishLabelRow = r
        pfSheet.write(avgFinishLabelRow, 1, "Average Finish", text_format_white)

        # Print the sorted members in one by one, with their place finishes
        c = 2
        r = 1
        memberCounter = 0
        # averagesByRace = [[]] # Holds all race scores for averages by race
        # iterate thru each member
        while memberCounter < self.numMembers:
            # Current place label
            pfSheet.write(r, c, int(memberCounter+1), place_format)
            r = r + 1

            # Fill in name and scores
            raceNum = 0
            memberColumn, avgFinish = sortedPlaceFinishes[memberCounter]
            for entry in memberColumn:
                # Name label
                if r == 2:
                    pfSheet.write(r, c, str(entry), name_label)
                # A place in some race
                else:
                    pfSheet.write(r, c, int(entry), race_score)
                    raceNum = raceNum + 1
                r = r + 1

            # Color cells gray between last race scored and the averages row
            fillRow = r
            while fillRow < avgFinishLabelRow:
                pfSheet.write(fillRow, c, " ", race_score)
                fillRow = fillRow + 1

            # Put the member's average score in the averages row
            pfSheet.write(avgFinishLabelRow, c, round(avgFinish, 2), averages_row_format)

            r = 1
            c = c + 1
            memberCounter = memberCounter + 1
        numCompletedRaces = raceNum


        # print("")
        # Print averages by race to the right of everything, if weve completed 1 or more actual race
        if numCompletedRaces > 0:

            # Set up the list of lists
            averagesByRace = [[]]
            i = 0
            while i < numCompletedRaces+1:
                tempList = []
                tempList.append("DUMMY")
                if i == 0:
                    averagesByRace[0] = tempList
                else:
                    averagesByRace.append(tempList)
                i = i + 1

            # print(averagesByRace)

            # Append race scores to each race list
            i=0
            # loop thru members
            while i < self.numMembers:
                memberColumn, avgFinish = sortedPlaceFinishes[i]

                # loop thru races, add to a list of lists
                j=1
                while j < len(memberColumn):
                    averagesByRace[j].append(memberColumn[j])
                    j=j+1

                i=i+1

            # delete dummy from each race, sum lists after list 0
            i = 1
            averages = []
            while i < len(averagesByRace):
                track, url = self.Races[i+2]
                averagesByRace[i].remove("DUMMY")
                # print("")
                #print(averagesByRace[i])
                avg = sum(averagesByRace[i])/self.numMembers
                # print(str(track) + " avg: " +  str(avg) + "/" + str(self.numMembers) + " = " + str(avg))
                averages.append(avg)
                i = i + 1

            # Print the averages in their respective cells
            r = 3
            i = 0
            c = 2 + self.numMembers
            while i < numCompletedRaces:
                pfSheet.write(r, c, round(averages[i], 1), averages_by_race)
                i = i + 1
                r = r + 1

            # Print the average of averages
            pfSheet.write(avgFinishLabelRow, c, round(sum(averages)/numCompletedRaces, 2), averages_row_format)

            # find the number of first place finishes everyone has
            self.findFirstPlaceFinishes()

            # print this info into the spreadsheet
            totalLeagueWins=0
            r = avgFinishLabelRow + 3
            c = 2
            for entry in self.firstPlaceFinishes:
                member, numFirstPlaceFinishes = entry

                # Print the members name, and color it depending on their number of wins
                if numFirstPlaceFinishes == 0:
                    pfSheet.write(r, c, str(member), zero_wins_white)
                elif numFirstPlaceFinishes == 1:
                    pfSheet.write(r, c, str(member), one_win_peach)
                elif numFirstPlaceFinishes == 2:
                    pfSheet.write(r, c, str(member), two_wins_lightBlue)
                elif numFirstPlaceFinishes == 3:
                    pfSheet.write(r, c, str(member), three_wins_yellowGreen)
                elif numFirstPlaceFinishes == 4:
                    pfSheet.write(r, c, str(member), four_wins_beer)
                elif numFirstPlaceFinishes == 5:
                    pfSheet.write(r, c, str(member), five_wins_lightSlateBlue)
                elif numFirstPlaceFinishes == 6:
                    pfSheet.write(r, c, str(member), six_wins_mangoOrange)
                elif numFirstPlaceFinishes == 7:
                    pfSheet.write(r, c, str(member), seven_wins_hotPink)
                elif numFirstPlaceFinishes == 8:
                    pfSheet.write(r, c, str(member), eight_wins_purpleDragon)
                else:
                    pfSheet.write(r, c, str(member), ninePlus_wins_red)

                # Print the members number of wins
                if numFirstPlaceFinishes > 0:
                    pfSheet.write(r+1, c, int(numFirstPlaceFinishes), text_format_white_center)
                else:
                    pfSheet.write(r+1, c, str(" "), text_format_white_center)

                totalLeagueWins = totalLeagueWins + numFirstPlaceFinishes
                c = c + 1

            # print the total number of wins in the league
            pfSheet.write(r+1, c, int(totalLeagueWins), text_format_white_center)

        # Show how the wildcard was broken down
        wildcardBreakdownRow = avgFinishLabelRow + 7
        r = wildcardBreakdownRow
        c = 1
        i = 0
        lastWasNewLine = False
        writeNames = False
        wildcardInfo = self.wildcardBreakdown
        pfSheet.write(wildcardBreakdownRow, c, "Wildcard Breakdown", wildcard_breakdown_title)
        while i < len(wildcardInfo):
            if wildcardInfo[i] == "NEW LINE":
                lastWasNewLine = True
                writeNames = False
                c = 1
                r = r + 1
                i = i + 1

            if lastWasNewLine:
                pfSheet.write(r, c, wildcardInfo[i], wildcard_breakdown_new_row)
                lastWasNewLine = False
                writeNames = True
                i = i + 1
                c = c + 1

            if writeNames:
                pfSheet.write(r, c, wildcardInfo[i], wildcard_breakdown_name)
                c = c + 1

            i = i + 1

        # Freeze the top columns
        pfSheet.freeze_panes(3, 0)

############################################################################################################################################################
############################################################################################################################################################

    #Find each members picks of the current race and put them in a list called Picks
    def findPicks(self):
        print_to_log("Finding user's picks..")

        #if the race num is 0, set an extra bit so we don't set a pick as member names
        extraBit = 0
        if self.currentRaceNum == 0:
            extraBit = 1

        df = self.gSheet.parse('Picks', na_values=['NA'], nrows=1, header = None)
        numParticipants = df.shape[1]
        #print(numParticipants)

        #Get the first member's name (in column C, there must be one member to have a league)
        dfName = self.gSheet.parse('Picks', usecols = "C", na_values=['NA'], nrows=1, header = None)
        col = dfName[2]
        name = []
        for i in col:
            if i != 'EMPTY':
                name.append(i)

        #Get their pick for the selected race
        if self.currentRaceNum+1 >= self.gap:
            dfPick = self.gSheet.parse('Picks', skiprows=self.currentRaceNum+1, usecols = "C", na_values=['NA'], nrows=1, header = None)
        else:
            dfPick = self.gSheet.parse('Picks', skiprows=self.currentRaceNum+extraBit, usecols = "C", na_values=['NA'], nrows=1, header = None)

        dfPick = dfPick.replace(np.nan, 'EMPTY', regex=True)
        col = dfPick[2]
        pick = []
        for i in col:
            #if i != 'EMPTY':
            pick.append(i)

        pick = name[0], pick[0]

        #append the pick to the list
        self.Picks.append(pick)

        #Loop through the rest of the members to get their picks
        c=3
        while c < numParticipants:
            c=c+1

            colLetter = findCol(c)
            if self.currentRaceNum+1 >= self.gap:
                dfPick = self.gSheet.parse('Picks', skiprows=self.currentRaceNum+1, usecols = colLetter, na_values=['NA'], nrows=1, header = None)
            else:
                dfPick = self.gSheet.parse('Picks', skiprows=self.currentRaceNum+extraBit, usecols = colLetter, na_values=['NA'], nrows=1, header = None)

            dfName = self.gSheet.parse('Picks', usecols = colLetter, na_values=['NA'], nrows=1, header = None)
            dfName = dfName.replace(np.nan, 'EMPTY', regex=True)
            dfPick = dfPick.replace(np.nan, 'EMPTY', regex=True)
            col = dfPick[c-1]
            driver = []
            for i in col:
                #if i != 'EMPTY':
                driver.append(i)

            col = dfName[c-1]
            name = []
            for i in col:
                #if i != 'EMPTY':
                name.append(i)

            pick = name[0], driver[0]
            #print(pick)
            self.Picks.append(pick)

############################################################################################################################################################
############################################################################################################################################################

    #Find each member's score based on their pick, and put in a list called Scores
    def findScores(self):
        #find scores based person's pick and results of race
        print_to_log("Finding scores based on pick..")

        i=0
        r=4
        c=3
        size=len(self.Picks)
        matchFound=0
        #scores = []
        pickedDrivers = []
        while i < size:
            name, pick = self.Picks[i]
            # print(name, pick)

            j=0
            numDrivers=len(self.Drivers)

            # if the member has picked this driver twice before (denoted with '*' on key sheet)
            if "*" not in pick:

                #loop thru drivers to find corresponding pts
                while j < numDrivers:
                    #get the current driver
                    driverName, pos, pts, penalty = self.Drivers[j]

                    #if the name isn't "driver"
                    if driverName not in "driver":

                        #find the drivers first two initials, and
                        #make his adjusted name this plus last name
                        firstTwo = driverName[0:2]
                        n = driverName.split()
                        adjustedName = firstTwo + " " + n[1]

                        #find the first two letters of pick
                        # print(pick)
                        firstTwo = pick[0:2]
                        firstTwo = re.sub('[!@#.,]', ' ', firstTwo)
                        n = pick.split()
                        s = len(n)

                        #If there's no pick, set to a meaningful garbage value
                        if s == 0:
                            adjustedPick = "NoPickPlaced"
                        else:
                            adjustedPick = n[0]

                        #if theres only a last name
                        if s == 1:
                            adjustedPick = n[0]

                            if adjustedPick.lower() == 'busch' or adjustedPick.lower() == 'dillon':
                                adjustedPick = "last name disc."

                        #if the person only entered in drivers first initial
                        s2 = len(firstTwo)
                        #print(firstTwo)
                        if firstTwo[1] == ' ' and s2 == 2:
                            #print("here")
                            firstTwo2 = driverName[0:1]
                            if firstTwo[0] == 'k':
                                adjustedName = 'K disc.'
                            else:
                                n2 = driverName.split()
                                adjustedName = firstTwo2[0] + " " + n2[1]
                                adjustedPick = firstTwo[0] + " " + n[1]

                        #if theres both a first and last name
                        if s == 2 and firstTwo[1] != " ":
                            adjustedPick = firstTwo + " " + n[1]
                            lastName = n[1]
                            lastName = re.sub('[!@#.,]', '', lastName)
                            if lastName.lower() in "jr":
                                adjustedPick = n[0]

                        #if they put in a specific first name
                        if pick.lower() == "kurt":
                            adjustedPick = "Ku Busch"
                        elif pick.lower() == "kyle":
                            adjustedPick = "Ky Busch"
                        elif pick.lower() == "chase":
                            adjustedPick = "Elliott"

                        # If theyre idiots and mispelled hard names
                        if pick.lower() == "elliot":
                            adjustedPick = "Elliott"
                        elif pick.lower() == "keslowski":
                            adjustedPick = "Keselowski"

                        #print(adjustedName.lower(), adjustedPick.lower())

                        #if the adjusted pick exists in the adjusted name
                        if adjustedPick.lower() in adjustedName.lower():

                            #insert the selected score into appropriate cell
                            matchFound=1

                            pickedDrivers.append(driverName)

                            #if the race is marked, score differently (10 - place finish, min of 0 points)
                            if self.markedRace == 1:
                                altScore = 10 - int(pos) + 1
                                if altScore < 0:
                                    altScore = 0
                                score = name, pick, int(altScore), int(pos)
                            #non-marked races are awarded the points given to them according to NASCAR rules
                            else:
                                score = name, pick, int(pts), int(pos)
                            self.Scores.append(score)
                            break
                    j=j+1

            #if there was no driver found or driver has '*', set the member's points to 0 and pos to numDrivers
            if matchFound == 0:
                    pts = 0
                    pos = numDrivers
                    score = name, pick, int(pts), int(pos)
                    self.Scores.append(score)

            # print(name, "'s score: ", score[2], " pick: ", score[1], " driver's place: ", score[3]) #print the score
            print_to_log(str(name + "'s score: " + str(score[2]) + ", pick: " + str(score[1]) + ", place: " + str(score[3])))
            matchFound=0
            i=i+1

        # Find unique names in pickedDrivers
        uniqueDrivers = []
        frequencies = []
        for driver in pickedDrivers:
            if driver not in uniqueDrivers:
                uniqueDrivers.append(driver)

        for driver in uniqueDrivers:
            occurences = pickedDrivers.count(driver)
            info = driver, occurences
            frequencies.append(info)

        # self.memberStatsFull = sorted(scoresTemp, key=itemgetter(1), reverse=True)
        self.driverFrequency = sorted(frequencies, key=itemgetter(1), reverse=True)
        print(self.driverFrequency)

############################################################################################################################################################
############################################################################################################################################################

    #Find each member's total points and store in a list called memberStatsFull
    def findPointTotals(self):
        #Open an xlsx for reading and writing
        masterBook = load_workbook(filename = "key_sheets\MasterResults.xlsx")
        masterResultsSheet = masterBook['MasterResults']

        #find everyone's points totals
        r=1
        c=3
        scoresTemp = []
        for value in self.Scores:
            name, pick, pts, pos = value
            #if this is their column
            colLetter = findCol(c)
            dfName = self.gSheet.parse('MasterResults', usecols = colLetter, na_values=['NA'], nrows=1, header = None)
            col = dfName[c-1]
            sheetName = []
            for i in col:
                #if i != 'EMPTY':
                sheetName.append(i)

            if name in str(sheetName[0]):
                #Get all the scores previously as a list
                totalScore=0
                dfScore = self.gSheet.parse('MasterResults', skiprows=1, usecols = colLetter, na_values=['NA'], header = None)
                dfScore = dfScore.replace(np.nan, 'EMPTY', regex=True)
                col = dfScore[c-1]
                score = []
                counter=0
                for i in col:
                    #print(i)
                    if i != 'EMPTY':
                        score.append(i)
                    elif counter+2 == self.gap:
                        score.append("GAP")
                    else:
                        score.append("EMPTY_CELL")

                    counter=counter+1

                #update master with last race
                #adjust for the gap
                if self.currentRaceNum+1 >= self.gap:
                    masterResultsSheet.cell(row = self.currentRaceNum+2, column = c).alignment = Alignment(horizontal='center')
                    masterResultsSheet.cell(row = self.currentRaceNum+2, column = c).value = int(pts)
                else:
                    if self.currentRaceNum == 0:
                        masterResultsSheet.cell(row = self.currentRaceNum+2, column = c).alignment = Alignment(horizontal='center')
                        masterResultsSheet.cell(row = self.currentRaceNum+2, column = c).value = int(pts)
                    else:
                        masterResultsSheet.cell(row = self.currentRaceNum+1, column = c).alignment = Alignment(horizontal='center')
                        masterResultsSheet.cell(row = self.currentRaceNum+1, column = c).value = int(pts)

                #print(name)
                #print(score)
                #print("")

                #loop through this list to find a total
                for val in score:
                    if val != "GAP" and val != "EMPTY_CELL":
                        totalScore = totalScore + val

                #Add on the current race to the total
                totalScore = totalScore + pts

                #Add the member to a list
                member = name, totalScore, pick, pts, score
                scoresTemp.append(member)


            c=c+1

        # sort the members on the total score (highest to lowest)
        self.memberStatsFull = sorted(scoresTemp, key=itemgetter(1), reverse=True)

        # set the size of the league
        self.numMembers = len(self.memberStatsFull)

        print_to_log("Saving master results and waiting for changes..")
        masterBook.save("key_sheets\MasterResults.xlsx")
        root.after(1500)

############################################################################################################################################################
############################################################################################################################################################

    def findPeopleWhoPaid(self):
        i=0

        #Get names of people and payment status' as two rows of data
        leagueSize = len(self.memberStatsFull)
        dfName = self.gSheet.parse('Payments', na_values=['NA'], nrows=1, header = None)
        dfPaid = self.gSheet.parse('Payments', na_values=['NA'], nrows=1, skiprows=1, header = None)
        dfName = dfName.replace(np.nan, 'NO-NAME', regex=True)
        dfPaid = dfPaid.replace(np.nan, '', regex=True)


        while i < leagueSize:
            member = ""
            paid =   ""

            #Find the name and payment status within data frame
            for m in dfName[i]:
                member = m

            for p in dfPaid[i]:
                paid = p

            N = member, paid

            #Add to list of payment status'
            self.paymentStatus.append(N)

            i=i+1

############################################################################################################################################################
############################################################################################################################################################

    def updatePlaceFinishesKey(self):
        # Open an xlsx for writing
        masterBook = load_workbook(filename = "key_sheets\MasterResults.xlsx")
        masterResultsSheet = masterBook['PlaceFinishes']

        # find where we currently are on the spreadsheet
        df = self.gSheet.parse('PlaceFinishes', usecols = "C", na_values=['NA'], header = None)
        df = df.replace(np.nan, 'EMPTY', regex=True)
        col = df[2]
        racesInSheet = []
        for i in col:
            if i != 'EMPTY':
                racesInSheet.append(i)

        curRow = len(racesInSheet) + 1
        c = 3

        # loop through each member's place finish and insert it into the key sheet
        for result in self.Scores:
            memberName, pick, pts, pos = result
            # print(memberName + ",  " + str(pos) + ", CELL: " + str(curRow) + "," + str(c))
            masterResultsSheet.cell(row = curRow, column = c).alignment = Alignment(horizontal='center')
            masterResultsSheet.cell(row = curRow, column = c).value = int(pos)
            c = c + 1

        masterBook.save("key_sheets\MasterResults.xlsx")

############################################################################################################################################################
############################################################################################################################################################

    def getMembersPlaceFinishes(self):
        # iterate through all members
        c = 3 # starting col
        i = 0 # member number/loop counter
        while i < self.numMembers:
            memberName, pick, pts, posLast = self.Scores[i]
            colLetter = findCol(c + i)
            df = self.gSheet.parse('PlaceFinishes', usecols = colLetter, na_values=['NA'], header = None)
            df = df.replace(np.nan, 'EMPTY', regex=True)
            col = df[c + i - 1]
            places = []
            for val in col:
                if val != 'EMPTY':
                    places.append(val)

            if self.markedRace != 1:
                places.append(posLast)

            # find average place finish for the member
            avg = 0
            if len(places) > 1:
                # NEED TO CHANGE SUM IF NO PICK
                s = sum(places[1:])
                avg = round((s / (len(places) - 1)), 2)

            # push information into PlaceFinishes[]
            information = places, avg
            self.PlaceFinishes.append(information)

            i = i + 1

        print(self.PlaceFinishes)
        print("")

############################################################################################################################################################
############################################################################################################################################################

    def findFirstPlaceFinishes(self):
        finishes = []

        # Loop through each member
        for entry in self.PlaceFinishes:
            numFirstPlaceFinishes = 0
            placesList, avgFinish = entry

            # Loop through their scores, tally their first place finishes
            for place in placesList:
                if place == 1:
                    numFirstPlaceFinishes = numFirstPlaceFinishes + 1

            # Append info to the firstPlaceFinishes list
            info = placesList[0], numFirstPlaceFinishes
            finishes.append(info)


        self.firstPlaceFinishes = sorted(finishes, key=itemgetter(1), reverse=True)
        # print(self.firstPlaceFinishes)

############################################################################################################################################################
############################################################################################################################################################

    def tiebreakChaseBorderMembers(self):
        # Find how many members are in the chase
        numInChase = math.floor(self.numMembers/2)
        if numInChase > 15:
            numInChase = 15

        # Get all members total scores and store in easier-to-work-with list
        scoresSorted = self.memberStatsFull

        # Get the score of the person on the border of the chase, and the person after
        name1, totalScore1, pick1, pts1, score1 = scoresSorted[numInChase-1]
        name2, totalScore2, pick2, pts2, score2 = scoresSorted[numInChase]

        print(scoresSorted)

        # If the scores are equal, we need to tiebreak all people with this score.
        # If not, just return, no tiebreak is needed
        if totalScore1 == totalScore2:
            # Create a list of the people before the tiebreak members
            listA = scoresSorted[:scoresSorted.index(totalScore1)]

############################################################################################################################################################
############################################################################################################################################################

    def findWildcard(self):
        # print("")

        # Find the number of people that can be in the wildcard pool
        numInChase = math.floor(self.numMembers/2)
        if numInChase > 15:
            numInChase = 15

        # Pull the list of these people from all entrants
        wildcardPool = []
        i = numInChase
        while i < self.numMembers:
            name, totalScore, pick, pts, scores = self.memberStatsFull[i]
            wildcardPool.append(name)
            i = i + 1

        # Grab everyones place finishes from this list, store in another list
        startingList = []
        for name in wildcardPool:

            # Loop through PlaceFinishes to get member's finishes
            for entry in self.PlaceFinishes:
                finishes, avgFinish = entry
                memberNameInFullList = finishes[0]

                if name == memberNameInFullList:
                    info = name, finishes[1:]
                    startingList.append(info)
                    break

        # Put pool in wildcard breakdown
        self.wildcardBreakdown.append("NEW LINE")
        self.wildcardBreakdown.append("Starting Pool:")
        for entry in startingList:
            name, finishes = entry
            self.wildcardBreakdown.append(name)

        # Find wildcard from this list by searching for the member with the most amount of wins, then 2nd place, etc..
        wildcardFound = 0
        maxWins = 0
        wildcardPool = []
        placeCounter = 1
        thirdLoopPlus = False
        while wildcardFound == 0:
            # print("")
            # print(startingList)
            wildcardPool = []

            # Find the max number of wins between everyone
            maxWins=0

            # If its not the 2nd time through, look at place finishes
            if placeCounter != 2:
                if thirdLoopPlus:
                    placeCounter = placeCounter - 1

                # loop thru members
                for entry in startingList:
                    name, finishes = entry
                    numFirstPlaceFinishes = 0

                    # find their number of wins
                    for place in finishes:
                        if place == 1:
                            numFirstPlaceFinishes = numFirstPlaceFinishes + 1

                    # if greater than maxWins, reset maxWins to new value
                    if numFirstPlaceFinishes > maxWins:
                        maxWins = numFirstPlaceFinishes

                # Append userful info to wildcardBreakdown
                self.wildcardBreakdown.append("NEW LINE")
                if maxWins == 1:
                    self.wildcardBreakdown.append( str( str(maxWins) + "   " + str(placeCounter) + getOrdinalFor(placeCounter)) )
                else:
                    self.wildcardBreakdown.append( str( str(maxWins) + "   " + str(placeCounter) + getOrdinalFor(placeCounter) + "s") )

                if thirdLoopPlus:
                    placeCounter = placeCounter + 1
            # If it is the second time through, look at points
            else:
                # loop thru members
                for entry in startingList:
                    name, finishes = entry
                    totalPoints = 0


                    # Find their point total
                    for stats in self.memberStatsFull:
                        nameInStats, totalScore, curPick, prevScores, curPoints = stats
                        if nameInStats == name:
                            totalPoints = totalScore
                            break

                    # if greater than maxWins, reset maxWins to new value
                    if numFirstPlaceFinishes > maxWins:
                        maxWins = totalPoints

                # Append userful info to wildcardBreakdown
                self.wildcardBreakdown.append("NEW LINE")
                self.wildcardBreakdown.append( str( str(int(maxWins)) + " " + "points") )
                thirdLoopPlus = True

            # Loop thru members again, if they have the max num of wins, append to wildcardPool
            for entry in startingList:
                name, finishes = entry
                numFirstPlaceFinishes = 0

                if placeCounter != 2:
                    for place in finishes:
                        if place == 1:
                            numFirstPlaceFinishes = numFirstPlaceFinishes + 1
                else:
                    # Find their point total
                    for stats in self.memberStatsFull:
                        nameInStats, totalScore, curPick, prevScores, curPoints = stats
                        if nameInStats == name:
                            numFirstPlaceFinishes = totalScore
                            break

                if numFirstPlaceFinishes == maxWins:
                    info = name, finishes
                    wildcardPool.append(info)
                    self.wildcardBreakdown.append(name)

            # if the length of the wildcardPool is 1, we found a wildcard
            if len(wildcardPool) == 1:
                name, scores = wildcardPool[0]
                self.wildcard = str(name)
                wildcardFound = 1
            else:
                # Loop through this pool, deleting the first place wins, and subtracting 1 from everything else.
                # This way, we can use the same method again until we find a wildcard. If the size of finishes
                # is 0, end the search, there is no wildcard
                startingList = []
                tempList = []
                for entry in wildcardPool:
                    name, finishes = entry

                    if placeCounter != 2:
                        tempList = []

                        for place in finishes:
                            score = place - 1
                            if score != 0:
                                tempList.append(score)

                    # If were looking at points, dont subtract 1
                    else:
                        tempList = finishes

                    info = name, tempList
                    startingList.append(info)

                # if the length of scores list is now 0, break.
                # FIND SOME RULE OR SET THE WILDCARD TO NOTHING
                if len(tempList) == 0:
                    # print("")
                    # print("final pool:")
                    # print(wildcardPool)
                    self.wildcard = "NONE"
                    wildcardFound = 1

            placeCounter = placeCounter + 1

        # print(self.wildcard)
        print(self.wildcardBreakdown)


###  END CLASS DEFS  #######################################################################################################################################
############################################################################################################################################################

# Finds the last race that appears online
def getRaceStatus():
    data = requests.get('http://www.espn.com/racing/results');
    #load the data into bs4
    soup = BeautifulSoup(data.text, 'html.parser')
    data = []
    URLs = []
    for tr in soup.find_all('tr'):
        for td in tr.find_all('td'):
            for b in td.find_all('b'):
                values = [a for a in b.find_all('a')]
                #print(values)
                data.append(values)

    #Scrape the soup for URLs
    i=0
    size = len(data)
    Races = []
    while i < size:
        v = str(data[i]) #get a line
        l = v.split('"')[1::2] #find URL inbetween quotes
        url = str(l)
        url = url[2:-2] #only get after first 2 chars and disregard last 2

        v = str(data[i]) #get the same line again
        track = v.split(">", 1)[1] #get everything after >
        track = track[:-5] #strip the last 5 chars

        #append the info to races struct
        race = track, url
        #skip over the moster energy open
        if track.lower() not in "monster energy open":
            Races.append(race)
            #print_to_log(str(race))
            #print(race)

        i=i+1

    return Races, len(Races)

def getNextSpreadsheetRace():
    gSheet = pd.ExcelFile('key_sheets\MasterResults.xlsx')
    df = gSheet.parse('MasterResults', usecols = "C", na_values=['NA'], header = None)
    df = df.replace(np.nan, 'EMPTY', regex=True)
    c = df[2].tolist()
    col = []
    for i in c:
        if i != 'EMPTY':
            col.append(i)
    currentRaceNum=len(col)
    currentRaceNum = currentRaceNum - 1
    # print(currentRaceNum)
    df = gSheet.parse('MasterResults', usecols = "B", na_values=['NA'], header = None)
    df = df.replace(np.nan, 'EMPTY', regex=True)
    c = df[1].tolist()
    del c[0]
    return str(c[currentRaceNum])

### GUI METHODS ###
#Insert a given text string into the log box in GUI
def print_to_log(text_string):
    logBox.insert(END, text_string + "\n")
    # Force the GUI to update
    root.update()

def clear_log_box():
    logBox.config(state=NORMAL)
    logBox.delete('1.0', END)
    root.update()
### END GUI METHODS ###

# Runs all calculations and facilitates the calculator
def runCalculations():
    clear_log_box()

    c = Calculator()
    c.findYear()
    c.findOnlineRaces()
    #print(c.Races)
    c.findCurSpreadsheetRace()
    #print(c.currentRaceNum, c.gap)
    print("")
    c.loadRaceResults()
    #print(c.Drivers)
    #print("")
    c.findPicks()
    #print(c.Picks)
    print("")
    c.findScores()
    #print(c.Scores)
    #print("")
    c.findPointTotals()
    #print("")
    #print("")
    #print(c.memberStatsFull)
    c.findPeopleWhoPaid()
    #print(c.paymentStatus[9][1])
    if c.markedRace != 1:
        c.updatePlaceFinishesKey()
    else:
        print("Marked Race. Not updating place finishes key.")
    print("")
    print("")
    c.getMembersPlaceFinishes()
    # c.tiebreakChaseBorderMembers()
    c.findWildcard()
    c.createWorkbook()


if __name__ == "__main__":
    #Make the GUI
    root = Tk()
    root.geometry("700x550")
    root.title("Shake-N-Bake Admin")

    #Give a title
    titleBanner = Label(root, text = "Shake-N-Bake Score Calculator", fg="red", font=("Times New Roman", 22))
    titleBanner.grid(row=0, column=1)

    # Display the version number
    versionBanner = Label(root, text = str("  Version: " + VERSION_NUMBER), fg="black", font=("Times New Roman", 11))
    versionBanner.grid(row=0, column=2)

    #Scrollbar and log textbox widgets
    scrollbar = Scrollbar(root)
    logBox = Text(root)

    logBox.focus_set()

    #Put them in the GUI
    logBox.grid(row=1, column=0, columnspan=3)
    scrollbar.grid(row=1, column=3, sticky='ns')

    #Configure them to work together
    scrollbar.config(command=logBox.yview)
    logBox.config(yscrollcommand=scrollbar.set)

    #Find the next available race
    availableRaces, numRaces = getRaceStatus()
    nextAvailRace = StringVar()
    nextAvailRace.set(str(availableRaces[numRaces-1][0]))

    #Display next available race
    nextRaceLabel = Label(root, text="Next Race Online:", fg="blue", justify=LEFT, width=20, font=("Times New Roman", 12), pady=10)
    nextRaceLabel.grid(row=2, column=0, sticky=W)
    nextRaceBanner = Label(root, textvariable=nextAvailRace)
    nextRaceBanner.grid(row=2, column=1, sticky=W)

    #Button to send command to make new spreadsheet and do work
    b = Button(root, text='Calculate', command=runCalculations)
    b.config(width=15, height=3)
    b.grid(row=3, column=2)

    #Find the next race on spreadsheet
    nextSprdshtRace = getNextSpreadsheetRace()
    nextAvailSprdshtRace = StringVar()
    nextAvailSprdshtRace.set(nextSprdshtRace)

    #Display next race to be scored on master
    nextSprdshtLabel = Label(root, text="Next Race On Spreadsheet:", fg="blue", justify=LEFT, width=20, font=("Times New Roman", 12))
    nextSprdshtLabel.grid(row=3, column=0, sticky=W)
    nextSprdshtBanner = Label(root, textvariable=nextAvailSprdshtRace)
    nextSprdshtBanner.grid(row=3, column=1, sticky=W)

    print_to_log("Press calculate to start..")

    root.mainloop()
