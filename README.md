# Shake-n-Bake Fantasy NASCAR Calculator

This project makes use of python virtualenvs. A useful guide to them can be found here:

https://docs.python-guide.org/dev/virtualenvs/


After cloning this project, set up a virtualenv in a directory of your choice as:
```
   > virtualenv \some\dir\virtualEnvName
```

(same on Unix just switch path slashes to '/')


After the virtualenv has been created, activate it with the following commands.

On Windows:
```
   > \some\dir\virtualEnvName\Scripts\activate
```

On Unix:
```
   $ source /some/dir/virtualEnvName/bin/activate
```


Once activated, install all packages found in requirements.txt using pip.

Finally, to run, use the following command:
```
  > python FantasyManager.py
```

  On Unix, it might be required to use:
```
  $ python3 FantasyManager.py
```


All development code should be done in the dev branch.

All major updates (new feature or big bug fix) should be a .1 increment to the
version number. Small bug fixes, small formatting changes, etc., should be a .01
increment to the version number.
